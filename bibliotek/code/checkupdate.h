//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkupdate
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcheckforupdates
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <qglobal.h>
#ifndef CHECKUPDATE_H
#define CHECKUPDATE_H

#include <QNetworkReply>
#include <QWidget>


#if defined(Q_OS_LINUX)
#if defined(CHECKUPDATE_LIBRARY)
#include "checkupdate_global.h"
class CHECKUPDATE_EXPORT CheckUpdate : public QWidget
#else
class CheckUpdate : public QWidget
#endif // CHECKUPDATE_LIBRARY
#else
class CheckUpdate : public QWidget
#endif // Q_OS_LINUX
{

    Q_OBJECT

private:
    int jfrVersion(const QString &currentVersion, const QString &newVersion);
    QString moreInfo(const QString *infoVersion);
    void networkError(const QString &progName, const QString &currentVersion, QIcon *icon);

    void updateNeeded(const QString &progName, const QString &currentVersion, const QString &onlyNewVersion, const QString &info, const QString &updateinstruktions, QIcon *icon);
    QString networkErrorMessages(QNetworkReply::NetworkError error);

signals:
//    CHECKUPDATE_EXPORT void foundUpdate(bool);
    void foundUpdate(bool);


public:
//    CHECKUPDATE_EXPORT void check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstruktions);
    void check(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstruktions, QIcon *icon);

//    CHECKUPDATE_EXPORT void checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstructions);
    void checkOnStart(const QString &progName, const QString &currentVersion, const QString &versionPath, const QString &updateinstructions, QIcon *icon);

    ~CheckUpdate();
    CheckUpdate();
};

#endif // CHECKUPDATE_H
