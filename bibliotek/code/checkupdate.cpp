//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkforupdate
//          Copyright (C) 2020 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcheckforupdates
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QMessageBox>
#include <QCheckBox>
#include <QIcon>
#include "checkupdate.h"

void CheckUpdate::check(const QString &progName,
                        const QString &currentVersion,
                        const QString &versionPath,
                        const QString &updateinstruktions,
                        QIcon *icon)
{
    // Q_INIT_RESOURCE(resource_checkupdate);
    QUrl url(versionPath);
    auto *nam = new QNetworkAccessManager(nullptr);
    QNetworkReply *reply = nam->get(QNetworkRequest(url));
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    connect(reply, &QNetworkReply::errorOccurred, this, [this, reply, progName, currentVersion, icon](QNetworkReply::NetworkError error) {
        reply->disconnect();
        QMessageBox *msgBox = new QMessageBox(nullptr);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(progName + " " + currentVersion);
        msgBox->setWindowIcon(*icon);
        msgBox->setText(networkErrorMessages(error));
        msgBox->exec();
        emit foundUpdate(false);
    });

#endif
#if (QT_VERSION < QT_VERSION_CHECK(5, 15, 0))
    connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>(&QNetworkReply::error), [ = ](QNetworkReply::NetworkError error) {
        reply->disconnect();
        QMessageBox *msgBox = new QMessageBox(nullptr);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(progName + " " + currentVersion);
        msgBox->setWindowIcon(*icon);
        msgBox->setText(networkErrorMessages(error));
        msgBox->exec();
        emit foundUpdate(false);
    });

#endif
    QObject::connect(
        reply, &QNetworkReply::finished, this,
        [this, reply, versionPath, progName, currentVersion,
    updateinstruktions, icon]() {
        QByteArray bytes = reply->readAll(); // bytes

        if(reply->error() != QNetworkReply::NoError) {
            QMessageBox *msgBox = new QMessageBox(nullptr);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(progName + " " + currentVersion);
            msgBox->setWindowIcon(*icon);
            msgBox->setText(tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
            msgBox->exec();
            emit foundUpdate(false);
            return;
        }

        QString newVersion(bytes);           // string
        newVersion = newVersion.trimmed();
        int index = newVersion.indexOf('\n');
        QString onlyNewVersion = newVersion.left(index);
        QString infoVersion = newVersion.mid(index);
        QString info = "";

        if(index > 0) {
            info = moreInfo(&infoVersion);
        }

        if(onlyNewVersion == "") {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(progName + " " + currentVersion);
            msgBox.setWindowIcon(*icon);
            msgBox.setText(tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
            msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
            msgBox.exec();
            emit foundUpdate(false);
            return;
        } else {
            int ver = jfrVersion(currentVersion, onlyNewVersion);

            if(ver == 0) {
                updateNeeded(progName, currentVersion, onlyNewVersion, info,
                             updateinstruktions, icon);
                emit foundUpdate(true);
                return;
            } else if(ver == 1) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Information);
                msgBox.setWindowTitle(progName + " " + currentVersion);
                msgBox.setWindowIcon(*icon);
                msgBox.setText(tr("Your version of ") + progName +
                               tr(" is newer than the latest official version. "));
                msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
                msgBox.exec();
                emit foundUpdate(false);
                return;
            } else if(ver == 2) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Information);
                msgBox.setWindowTitle(progName + " " + currentVersion);
                msgBox.setWindowIcon(*icon);
                msgBox.setText(tr("You have the latest version of ") + progName + ".");
                msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
                msgBox.exec();
                emit foundUpdate(false);
                return;
            } else {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Information);
//                msgBox.setWindowIcon(QIcon(":/images/update.png"));
                msgBox.setWindowTitle(progName + " " + currentVersion);
                msgBox.setWindowIcon(*icon);
                msgBox.setText(tr("There was an error when the version was checked."));
                msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
                msgBox.exec();
                emit foundUpdate(false);
                return;
            }
        }

        close();
    });
}

void CheckUpdate::checkOnStart(const QString &progName,
                               const QString &currentVersion,
                               const QString &versionPath,
                               const QString &updateinstruktions,
                               QIcon *icon)
{
    QUrl url(versionPath);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
    connect(reply, &QNetworkReply::errorOccurred, [reply]() {
        reply->disconnect();
        return;
    });

#endif
    QObject::connect(
        reply, &QNetworkReply::finished, this,
        [this, reply, versionPath, progName, currentVersion,
    updateinstruktions, icon]() {
        QByteArray bytes = reply->readAll(); // bytes

        if(reply->error() != QNetworkReply::NoError) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(progName + " " + currentVersion);
            msgBox->setWindowIcon(*icon);
            msgBox->setText(tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
            msgBox->exec();
            emit foundUpdate(false);
            return;
        }

        QString newVersion(bytes);           // string
        newVersion = newVersion.trimmed();
        int index = newVersion.indexOf('\n');
        QString onlyNewVersion = newVersion.left(index);
        QString infoVersion = newVersion.mid(index + 1);
        QString info = "";

        if(index > 0) {
            info = moreInfo(&infoVersion);
        }

        if(onlyNewVersion == "") {
            networkError(progName, currentVersion, icon);
            return;
        } else {
            int ver = jfrVersion(currentVersion, onlyNewVersion);

            if(ver == 0) {
                updateNeeded(progName, currentVersion, onlyNewVersion, info,
                             updateinstruktions, icon);
                emit foundUpdate(true);
                close();
            } else if(ver == 3) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Information);
                msgBox.setWindowIcon(*icon);
                msgBox.setWindowTitle(progName + " " + currentVersion);
                msgBox.setText(tr("\nThere was an error when the version was checked."));
                msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
                msgBox.exec();
                emit foundUpdate(false);
                return;
            } else {
                emit foundUpdate(false);
            }
        }
    });

    close();
}

int CheckUpdate::jfrVersion(const QString &currentVersion,
                            const QString &newVersion)
{
    int newVer;
    int currentVer;
    bool ok = true;
    QStringList listCurrentVersion = currentVersion.split(".");
    QStringList listNewVersion = newVersion.split(".");

    for(int i = 0; i <= 2; i++) {
        currentVer = listCurrentVersion.at(i).toInt(&ok, 10);
        newVer = listNewVersion.at(i).toInt(&ok, 10);

        if(!ok)
            return 3;

        if(newVer > currentVer)
            return 0;

        if(newVer < currentVer)
            return 1;
    }

    return 2;
}

QString CheckUpdate::moreInfo(const QString *infoVersion)
{
    QStringList listInfo;
    listInfo = infoVersion->split('\n');
    QString news;

    if(!listInfo.empty()) {
        news = "<br><br><i>" + tr("Updates:") + "</i>";
        news += "<ol>";

        for(int i = 0; i < listInfo.size(); i++) {
            news += "<li>" + listInfo.at(i) + "</li>";
        }
    } else {
        news = "";
    }

    news += "</ol>";
    return news;
}

void CheckUpdate::updateNeeded(const QString &progName,
                               const QString &currentVersion,
                               const QString &onlyNewVersion,
                               const QString &info,
                               const QString &updateinstruktions,
                               QIcon *icon)
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowIcon(*icon);
    msgBox.setWindowTitle(progName + " " + currentVersion);
    msgBox.setText(QStringLiteral("<h2 style=\"color:green\">") +
                   tr("There is a new version of ") + progName +
                   QStringLiteral("</h2><h3 style=\"color:green\">") +
                   tr("Latest version: ") + onlyNewVersion +
                   QStringLiteral("</h3<p>") + updateinstruktions + info + QStringLiteral("</p>"));
    msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
    msgBox.exec();
}

void CheckUpdate::networkError(const QString &progName, const QString &currentVersion, QIcon *icon)
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setWindowTitle(progName + " " + currentVersion);
    msgBox.setWindowIcon(*icon);
    msgBox.setText(tr("No Internet connection was found.\nPlease check your Internet settings and firewall."));
    msgBox.addButton(tr("Ok"), QMessageBox::ActionRole);
    msgBox.exec();
    emit foundUpdate(false);
    return;
}

CheckUpdate::CheckUpdate() {}

CheckUpdate::~CheckUpdate() {}
