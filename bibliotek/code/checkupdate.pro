#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          checkforupdate
#//          Copyright (C) 2020 - 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/libcheckforupdates
#//          programming@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

# 2021-02-19 00:34
QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# CONFIG: LIBS += -L../lib5/ -lcrypto
# CONFIG: LIBS += -L../lib5/ -lssl

HEADERS += \
    checkupdate.h \
    checkupdate_global.h

SOURCES += \
    checkupdate.cpp \
    networkerrormessages.cpp




#Export LIBS
# DEFINES += CHECKUPDATE_LIBRARY

TEMPLATE = lib
#TEMPLATE = app

# By default, qmake will make a shared library. Uncomment to make the library
# static.
win32:CONFIG += staticlib
unix:CONFIG+=staticlib

#CONFIG += lrelease embed_translations

# By default, TARGET is the same as the directory, so it will make 
# liblibrary.so or liblibrary.a (in linux).  Uncomment to override.


CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG






equals(BUILD,RELEASE) {

    TARGET=checkupdate

}
equals(BUILD,DEBUG) {

    TARGET=checkupdated
}

equals(QT_MAJOR_VERSION, 5) {
DESTDIR=$$OUT_PWD-lib
#DESTDIR=../../program/lib5
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib
}

#CONFIG += /opt/Qt/5.15.2/gcc_64/bin/lrelease embed_translations



#UI_DIR = ../code

TRANSLATIONS += i18n/_libcheckupdate_template_xx_XX.ts \
                i18n/_libcheckupdate_sv_SE.ts \
                i18n/_libcheckupdate_it_IT.ts

RESOURCES += \
    resource_checkupdate.qrc



message (--------------------------------------------------)
message (OS: $$QMAKE_HOST.os)
message (Arch: $$QMAKE_HOST.arch)
message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (qmake path: $$QMAKE_QMAKE)
message (Qt version: $$QT_VERSION)
message(*.pro path: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
message(PWD: $$PWD)
message (--------------------------------------------------)




