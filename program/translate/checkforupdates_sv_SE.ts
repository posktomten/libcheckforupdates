<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckForUpdates</name>
    <message>
        <location filename="../checkforupdates.cpp" line="49"/>
        <location filename="../checkforupdates.cpp" line="121"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="66"/>
        <location filename="../checkforupdates.cpp" line="74"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="67"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="75"/>
        <source> is the same version as the latest official version. </source>
        <translation> är samma version som den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="84"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="136"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="177"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="198"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkforupdates.cpp" line="200"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
</context>
</TS>
