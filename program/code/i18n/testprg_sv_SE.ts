<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE" sourcelanguage="en">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Test Program</source>
        <translation>Testprogram</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="22"/>
        <source>Check on start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="49"/>
        <source>Check for updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="29"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="40"/>
        <source>Compiled </source>
        <translation>Kompilerad </translation>
    </message>
    <message>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation type="vanished">Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <source>Download a new</source>
        <translation type="vanished">Ladda ner en ny</translation>
    </message>
    <message>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update component&quot;.</source>
        <translation type="vanished">Välj &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; och &quot;Update component&quot;.</translation>
    </message>
    <message>
        <source>true sent from library</source>
        <translation type="vanished">true skickat från bibliotek</translation>
    </message>
    <message>
        <source>false sent from library</source>
        <translation type="vanished">false skickat från bibliotek</translation>
    </message>
    <message>
        <source>At start: true sent from library</source>
        <translation type="vanished">Vid start: true skickat från bibliotek</translation>
    </message>
    <message>
        <source>At start: false sent from library</source>
        <translation type="vanished">Vid start: false skickat från bibliotek</translation>
    </message>
    <message>
        <source>Found updates</source>
        <translation type="vanished">Hittade uppdateringar</translation>
    </message>
    <message>
        <source>Found updates at program startup</source>
        <translation type="vanished">Hittade uppdateringar när programmet startade</translation>
    </message>
    <message>
        <source>Found update</source>
        <translation type="vanished">Hittade uppdateringar</translation>
    </message>
    <message>
        <source>found update at program startup</source>
        <translation type="vanished">hittade uppdateringar när programmet startade</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../dialog.cpp" line="42"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation type="unfinished">Välj &quot;Verktyg&quot;, &quot;Uppdatera&quot; för att uppdatera.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="47"/>
        <source>Download a new</source>
        <translation type="unfinished">Ladda ner en ny</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="51"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update component&quot;.</source>
        <translation type="unfinished">Välj &quot;Verktyg&quot;, &quot;Underhållsverktyg&quot; och &quot;Update component&quot;.</translation>
    </message>
</context>
</TS>
