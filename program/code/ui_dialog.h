/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 6.4.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QCheckBox *chOnStart;
    QLabel *lblCompile;
    QSpacerItem *verticalSpacer;
    QPushButton *pbCheck;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName("Dialog");
        Dialog->resize(427, 122);
        gridLayout = new QGridLayout(Dialog);
        gridLayout->setObjectName("gridLayout");
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName("verticalLayout");
        chOnStart = new QCheckBox(Dialog);
        chOnStart->setObjectName("chOnStart");

        verticalLayout->addWidget(chOnStart);

        lblCompile = new QLabel(Dialog);
        lblCompile->setObjectName("lblCompile");

        verticalLayout->addWidget(lblCompile);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        pbCheck = new QPushButton(Dialog);
        pbCheck->setObjectName("pbCheck");

        verticalLayout->addWidget(pbCheck);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "Test Program", nullptr));
        chOnStart->setText(QCoreApplication::translate("Dialog", "Check on start", nullptr));
        lblCompile->setText(QCoreApplication::translate("Dialog", "TextLabel", nullptr));
        pbCheck->setText(QCoreApplication::translate("Dialog", "Check for updates", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
