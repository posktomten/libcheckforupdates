#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          checkforupdates
#//          Copyright (C) 2020 - 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/libcheckforupdates
#//          programming@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3
#//   as published by the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>




QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = testprg



# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    dialog.cpp 

HEADERS += \
    dialog.h 
 

FORMS += \
    dialog.ui

TRANSLATIONS += \
    i18n/testprg_sv_SE.ts
    
UI_DIR = ../code


equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug

}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"

CONFIG (release, debug|release): LIBS += -L../lib6/ -lcheckupdate # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcheckupdated # Debug

}

INCLUDEPATH += "../include"
DEPENDPATH += "../include"

UI_DIR = ../code
#win32:RC_ICONS += images/icon.ico

RESOURCES += \
    resource.qrc
