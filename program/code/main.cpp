//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkforupdates
//          Copyright (C) 2020 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcheckforupdates
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "dialog.h"
#include <QApplication>
//#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    QTranslator translator;
//    const QString translationPath = ":/i18n/complete.qm";
//    if(translator.load(translationPath)) {
//        QApplication::installTranslator(&translator);
//    }
    QString fontPath = ":/font/Ubuntu-Regular.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);

    if(fontId != -1) {
        QFont f("Ubuntu", FONT);
        a.setFont(f);
    }

    Dialog w;
    w.show();
    return a.exec();
}
