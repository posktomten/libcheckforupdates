//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkforupdates
//          Copyright (C) 2020 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcheckforupdates
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "dialog.h"
#include "ui_dialog.h"
//#include <QMessageBox>
//Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog)
Dialog::Dialog(QWidget *parent) : QDialog(parent)
{
    Ui::Dialog *ui = new Ui::Dialog;
    ui->setupUi(this);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "Test Program", "checkupdate");
    settings.beginGroup("Settings");
    int checkstate = settings.value("checkstate").toInt();
    settings.endGroup();
//    qInfo() << checkstate;

    if(checkstate == 0) {
        ui->chOnStart->setChecked(false);
    } else {
        ui->chOnStart->setChecked(true);
    }

    move(screen()->geometry().center() - frameGeometry().center());
    this->setWindowIcon(QIcon(":/images/test.png"));
    this->setWindowTitle(DISPLAY_NAME " " VERSION);
    this->setMinimumWidth(400);
    ui->lblCompile->setText(tr("Compiled ") + BUILD_DATE_TIME + ", Qt" + QT_VERSION_STR);
#ifdef Q_OS_LINUX
    QString *updateinstructions = new QString(QObject::tr("Select \"Tools\", \"Update\" to update."));
#endif
#ifdef Q_OS_WIN
#ifdef portable
    QString *updateinstructions = new QString(
        QObject::tr("Download a new") + " <a href=\"" DOWNLOAD_PATH "\"> portable</a>");
#endif
#ifndef portable
    QString *updateinstructions = new QString(
        QObject::tr("Select \"Tools\", \"Maintenance Tool\" and \"Update component\"."));
#endif
#endif
    connect(ui->chOnStart, &QCheckBox::stateChanged, [this, ui] {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "Test Program", "checkupdate");
        settings.beginGroup("Settings");

        if(ui->chOnStart->isChecked())
        {
            settings.setValue("checkstate", 1);
//            qInfo() << "Ikryssad";
        } else
        {
            settings.setValue("checkstate", 0);
//            qInfo() << "INTE Ikryssad";
        }
        settings.endGroup();
//        qDebug() << ui->chOnStart->checkState();
    });

    if(ui->chOnStart->isChecked()) {
        checkOnStart(updateinstructions);
    }

    connect(ui->pbCheck, &QPushButton::clicked, [updateinstructions, this]() {
        CheckUpdate *cu = new CheckUpdate;
        cu->check(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
//        connect(cu, &CheckUpdate::foundUpdate, []() {
    });
//        connect(cu, &CheckUpdate::foundUpdate, [](bool b) {
//            if(b) {
//                QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION, tr("libcheckforupdates returned true"));
//            } else {
//                QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION, tr("libcheckforupdates returned false"));
//            }
//        });
//    });
}

void Dialog::checkOnStart(QString *updateinstructions)
{
    CheckUpdate *cu = new CheckUpdate;
//    connect(cu, &CheckUpdate::foundUpdate, [](bool b) {
//    CheckUpdate::connect(cu, &CheckUpdate::foundUpdate, []() {
//        if(b) {
//            QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION, tr("libcheckforupdates returned true at program startup"));
//        } else {
//            QMessageBox::information(nullptr, DISPLAY_NAME " " VERSION, tr("libcheckforupdates returned false at program startup"));
//        }
//    });
    cu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
}


Dialog::~Dialog()
{
//    delete ui;
}
