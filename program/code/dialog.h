//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          checkforupdates
//          Copyright (C) 2020 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/libcheckforupdates
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef DIALOG_H
#define DIALOG_H

#include "checkupdate.h"
#include "checkupdate_global.h"
#include <QDialog>
#include <QFontDatabase>
#include <QScreen>
#include <QSettings>


#define DISPLAY_NAME "checkForUpdates"
#define VERSION "0.0.3"
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/appimagehelper/-/wikis/DOWNLOADS"
#define VERSION_PATH "http://bin.ceicer.com/checkforupdates/version.txt"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#ifdef Q_OS_LINUX
#define FONT 13
#endif
#ifdef Q_OS_WIN
#define FONT 10
#endif

QT_BEGIN_NAMESPACE
namespace Ui
{
class Dialog;
}
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

private:
//    Ui::Dialog *ui;
//    CheckUpdate *cu;
    void checkOnStart(QString *updateinstructions);
};
#endif // DIALOG_H
